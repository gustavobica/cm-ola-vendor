package com.gustavobica.www.olavendedor.parsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;

/**
 * Created by Gustavo Bica on 17/03/2016.
 */
public class WeatherParser {
    private String json;
    private String description;
    private String main;
    private String max;
    private String min;
    private String humidity;
    private String temp;
    JSONObject jsonObj=null;
    JSONObject nodeRoot=null;
    private String icon=null;

    public WeatherParser(String json) {
        this.json = json;
        try {
            DecimalFormat df = new DecimalFormat("#.00");
            jsonObj=new JSONObject(json);
            JSONArray jsonArray = jsonObj.getJSONArray("weather");
            nodeRoot = (JSONObject) jsonArray.get(0);
            description = nodeRoot.getString("description");
            icon = nodeRoot.getString("icon");
            main=nodeRoot.getString("main");
            JSONObject data = jsonObj.getJSONObject("main");
            double maxvalue=data.getDouble("temp_max");
            maxvalue=(maxvalue/10);
            max=df.format(maxvalue);
            double minvalue=data.getDouble("temp_min");
            min =df.format(minvalue/10);
            Double humidityvalue=data.getDouble("humidity");
            humidity = Double.toString(humidityvalue);
            double tempvalue=data.getDouble(("temp"));
            temp = df.format(tempvalue/10);

        }catch(JSONException e)
        {
            System.out.println(e);
        }
    }
    public String getDescription()
    {

        return description;
    }
    public String getMain()
    {

        return main;
    }
    public double convertFhToCel(double temp)
    {
        return (((temp - 32) * 5) / 9);
    }
    public String GetTemp()
    {

        return temp;
    }
    public String GetTemp_min()
    {

        return min;
    }
    public String getIcon()
    {
        return icon;
    }
    public String GetTemp_max()
    {
        return max;
    }
    public String GetHumidity()
    {
        return humidity;
    }

}
