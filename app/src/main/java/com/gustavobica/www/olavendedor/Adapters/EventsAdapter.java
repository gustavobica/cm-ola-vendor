package com.gustavobica.www.olavendedor.Adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gustavobica.www.olavendedor.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;
/**
 * Created by Gustavo Bica on 01/04/2016.
 */
public class EventsAdapter extends BaseAdapter {



        private static final int TYPE_ITEM = 0;
        private static final int TYPE_SEPARATOR = 1;
        private Typeface type;
        private Typeface type_bold;
        private ArrayList<String> mData = new ArrayList<String>();
        private ArrayList<String[]> list = new ArrayList<String[]>();
        private TreeSet<Integer> sectionHeader = new TreeSet<Integer>();
        private final int WHITE=0xffffffff;
        private Context context;
        private LayoutInflater mInflater;

        public EventsAdapter (Context context,ArrayList<String[]> list) {
            mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.list=list;
            this.context=context;

        }
        public EventsAdapter (Context context, Typeface font,Typeface bold,ArrayList<String[]> list) {
            mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            type=font;
            type_bold=bold;
            this.list=list;
        }
        public void addItem(final String item) {
            mData.add(item);
            notifyDataSetChanged();
        }
        public void addItemArrayList(final ArrayList<String[]> item) {
            list=item;
            notifyDataSetChanged();
        }



        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public String getItem(int position) {
            String rturn;

            rturn=list.get(position)[position];
            return rturn;
        }

        @Override
        public long getItemId(int position) {
            long pos=0;


            return pos;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder=new ViewHolder();
            View v = convertView;
            if (v == null) {

                v = mInflater.inflate(R.layout.event_item, null);


                //v.setBackgroundColor(WHITE);


            }
            if(list.size()<=0)
            {

                holder.Title.setText("Não existem Eventos");
                holder.Description.setVisibility(View.GONE);
                holder.Where.setVisibility(View.GONE);
                holder.When.setVisibility(View.GONE);
                holder.img.setVisibility(View.GONE);
            }else {
                try {

                    holder.Title = (TextView) v.findViewById(R.id.textTitleEvent);
                    holder.Description = (TextView) v.findViewById(R.id.textDescriptionEvent);
                    holder.Where = (TextView) v.findViewById(R.id.textWhere);
                    holder.When = (TextView) v.findViewById(R.id.textWhen);
                    holder.img = (ImageView) v.findViewById(R.id.imageViewEvents);
                    holder.Title.setText(list.get(position)[0]);
                   // holder.Description.setText(list.get(position)[1]);
                    holder.url = list.get(position)[4];
                    holder.Where.setText(list.get(position)[3]);
                    holder.When.setText(list.get(position)[2]);
                    holder.Title.setVisibility(View.VISIBLE);
                    holder.Description.setVisibility(View.GONE);
                    holder.Where.setVisibility(View.VISIBLE);
                    holder.When.setVisibility(View.VISIBLE);
                    holder.img.setVisibility(View.VISIBLE);
                }catch (Exception e)
                {
                 System.out.println("AQQQQQ"+e);
                }

                try {
                    if(!holder.url.equals("none")) {
                        Picasso.with(context).load(holder.url).into(holder.img);
                    }
                    else{
                        holder.img.setImageResource(R.drawable.events);
                    }
                } catch (Exception e) {
                    try{
                    holder.img.setImageResource(R.drawable.events);
                    System.out.println("adapter "+e);
                    }catch (Exception ex)
                    {

                    }
                }
            }
            return v;
        }

        public static class ViewHolder {
            public String url;
            public TextView Title;
            public TextView Description;
            public TextView Where;
            public TextView When;
            public ImageView img;
        }

    }



