package com.gustavobica.www.olavendedor.Fragments.SellFragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.gustavobica.www.olavendedor.Main.MainActivity;
import com.gustavobica.www.olavendedor.R;
import com.gustavobica.www.olavendedor.database.IcreamDatabase;

import java.util.ArrayList;

/**
 * Created by Gustavo Bica on 03/04/2016.
 */
public class Detail_IcreamFragment extends Fragment {
    Button Refill;
    PieChart piechartProfit;
    PieChart piechartSales;
    View rootView;
    String Current;
    IcreamDatabase dbhelper;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.detail_ice_cream, container, false);

        Refill = (Button) rootView.findViewById(R.id.buttonRefill);

        dbhelper = new IcreamDatabase(getActivity().getBaseContext());
        piechartProfit = (PieChart) rootView.findViewById(R.id.pieChartProfit);
        piechartSales = (PieChart) rootView.findViewById(R.id.pieChartSales);

        updateCharts("corneto morango");
        Refill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).UpdateToSalesView();

            }
        });


        Refill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dbhelper.ReStock(Current);
                ((MainActivity)getActivity()).UpdateStock(Current);
                System.out.println("Pressed Refill");
            }
        });


        return rootView;
    }
    public void sendIceToDetail(String Name){}
    public void updateCharts(String Name)
    {
        Current=Name;
        Cursor rs=dbhelper.getAllIcream();
        if(rs.moveToFirst()) {
            int i=0;
            String NameIce;
            int TotalSale=0;
            int TotalProfit=0;
            int Price;
            int Sales;
            ArrayList<Entry> entriesProfit = new ArrayList<>();
            ArrayList<Entry> entriesSales = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            do {
                NameIce=rs.getString(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_NAME));

                Price=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_PRICE));
                Sales=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_SALES));

                if(NameIce.equals(Name))
                {
                    entriesProfit.add(new Entry(Price * Sales, 0));
                    entriesSales.add(new Entry(Sales,0));
                    labels.add(NameIce);
                }
                else{
                    TotalProfit=TotalProfit+(Price*Sales);
                    TotalSale=TotalSale+Sales;

                }


                // dbhelper.madeSale(NameIce);
                i++;
            } while (rs.moveToNext());
            labels.add("Outros");
            entriesProfit.add(new Entry(TotalProfit, 1));
            entriesSales.add(new Entry(TotalSale,1));
            PieDataSet dataSetSales = new PieDataSet(entriesSales,"# of Sales");
            PieDataSet datasetProfit = new PieDataSet(entriesProfit, "# of Euros");
            dataSetSales.setColors(ColorTemplate.COLORFUL_COLORS);
            datasetProfit.setColors(ColorTemplate.COLORFUL_COLORS);
            PieData data = new PieData(labels, dataSetSales);
            PieData data1= new PieData(labels,datasetProfit);
            piechartSales.setData(data); // set the data and list of lables into chart
            piechartSales.setDescription("Vendas Totais");
            piechartProfit.setData(data1);
            piechartProfit.setDescription("Lucro Total");
            piechartProfit.invalidate();
            piechartSales.invalidate();


        }
    }
}