package com.gustavobica.www.olavendedor.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gustavobica.www.olavendedor.Main.MainActivity;
import com.gustavobica.www.olavendedor.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by Gustavo Bica on 04/04/2016.
 */
public class BuyListAdapter extends BaseAdapter {



    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;
    private Typeface type;
    private Typeface type_bold;
    private ArrayList<String> mData = new ArrayList<String>();
    private ArrayList<String[]> list = new ArrayList<String[]>();
    private TreeSet<Integer> sectionHeader = new TreeSet<Integer>();
    private final int WHITE=0xffffffff;
    private Context context;
    private LayoutInflater mInflater;
    private final int pos=0;

    public BuyListAdapter (Context context,ArrayList<String[]> list) {
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list=list;
        this.context=context;

    }
    public BuyListAdapter (Context context, Typeface font,Typeface bold,ArrayList<String[]> list) {
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        type=font;
        type_bold=bold;
        this.list=list;
    }
    public void addItem(final String item) {
        mData.add(item);
        notifyDataSetChanged();
    }
    public void addItemArrayList(final ArrayList<String[]> item) {
        list=item;
        notifyDataSetChanged();
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public String getItem(int position) {
        String rturn;

        rturn=list.get(position)[position];
        return rturn;
    }

    @Override
    public long getItemId(int position) {
        long pos=0;


        return pos;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=new ViewHolder();
        View v = convertView;
        if (v == null) {

            v = mInflater.inflate(R.layout.buy_list_item, null);


            //v.setBackgroundColor(WHITE);


        }
        if(list.size()<=0)
        {

            holder.Name.setText("Não existem Eventos");
            holder.Qty.setVisibility(View.GONE);
            holder.Price.setVisibility(View.GONE);
            holder.Cancel.setVisibility(View.GONE);

        }else {
            try {

                holder.Name = (TextView) v.findViewById(R.id.textBuyName);
                holder.Qty = (TextView) v.findViewById(R.id.textBuyQty);
                holder.Price = (TextView) v.findViewById(R.id.textBuyPrice);
                holder.Cancel = (Button) v.findViewById(R.id.buttonItemCancel);

                holder.Name.setText(list.get(position)[0]);
                holder.Price.setText(list.get(position)[2]);
                holder.Qty.setText(list.get(position)[1]);
                holder.Name.setVisibility(View.VISIBLE);
                holder.Qty.setVisibility(View.VISIBLE);
                holder.Price.setVisibility(View.VISIBLE);
                holder.Cancel.setVisibility(View.GONE);

                holder.Cancel.setOnClickListener(new View.OnClickListener() {
                //pos=position;
                    @Override
                    public void onClick(View v) {
                        System.out.println("Tentou cancelar");
                       // ((MainActivity)v.getContext()).sendIceToDetail(list.get(pos)[0]);
                    }
                });
            }catch (Exception e)
            {
                System.out.println("Buy List adapter:"+e);
            }

        }
        return v;
    }

    public static class ViewHolder {

        public TextView Name;
        public TextView Qty;
        public TextView Price;
        public Button Cancel;

    }

}



