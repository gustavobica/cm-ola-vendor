package com.gustavobica.www.olavendedor.IntentService;

/**
 * Created by Gustavo Bica on 17/03/2016.
 */



    import java.io.BufferedReader;
    import java.io.File;
    import java.io.FileOutputStream;
    import java.io.IOException;
    import java.io.InputStream;
    import java.io.InputStreamReader;
    import java.net.URL;

    import android.app.Activity;
    import android.app.IntentService;
    import android.content.Intent;
    import android.net.Uri;
    import android.os.Bundle;
    import android.os.Environment;
    import android.os.Message;
    import android.os.Messenger;
    import android.util.Log;

public class GetService extends IntentService {

        private int result = Activity.RESULT_CANCELED;
        public static final String URL = "urlpath";
        public static final String DATA = "DATA";
    public static final String TYPE = "TYPE";
        public static final String FILEPATH = "filepath";
        public static final String RESULT = "result";
        public static final String NOTIFICATION = " com.gustavobica.www.olavendedor.service.receiver";

        public GetService() {
            super("GetService");
        }

        // will be called asynchronously by Android
        @Override
        protected void onHandleIntent(Intent intent) {
            String urlPath = intent.getStringExtra(URL);
            String type = intent.getStringExtra(TYPE);
            InputStream stream=null;
            String json=null;
            System.out.println(type);
            if(type.equals("weather"))
            {
                type="weather";
                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                 System.out.println("Weather: "+e);
                }
            }else if(type.equals("events"))
            {
                type="events";
                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                    System.out.println("events Json:"+e);
                }

            }
            else if(type.equals("login"))
            {
                type="login";
                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                    System.out.println("events Json:"+e);
                }

            }
            else if(type.equals("logout"))
            {
                type="logout";
                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                    System.out.println("logout Json:"+e);
                }

            }
            else if(type.equals("getStock"))
            {
                type="getStock";
                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                    System.out.println("stock Json:"+e);
                }

            }
            else if(type.equals("findLocation"))
            {
                type="findLocation";
                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                    System.out.println("find Json:"+e);
                }

            }
            else if(type.equals("clearRequest"))
            {
                type="clearRequest";
                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                    System.out.println("Clear Request Json:"+e);
                }

            }
            else if(type.equals("GetRequests"))
            {
                type="GetRequests";

                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                    System.out.println("GetRequest Json:"+e);
                }

            }
            else if(type.equals("PostLocation"))
            {
                type="PostLocation";
                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                    System.out.println("PostLocation Json:"+e);
                }

            }
            else if(type.equals("GetChat"))
            {
                type="GetChat";
                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                    System.out.println("GetChat Json:"+e);
                }

            }else if(type.equals("GetID"))
            {
                type="GetID";

                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                    System.out.println("GetID Json:"+e);
                }

            }
            else if(type.equals("PostStock"))
            {
                type="PostStock";

                try {
                    URL url = new URL(urlPath);
                    stream = url.openConnection().getInputStream();


                    BufferedReader bR = new BufferedReader(new InputStreamReader(stream));
                    String line = "";

                    StringBuilder responseStrBuilder = new StringBuilder();
                    while ((line = bR.readLine()) != null) {

                        responseStrBuilder.append(line);
                    }
                    stream.close();

                    json = responseStrBuilder.toString();
                }catch(IOException e)
                {

                    System.out.println("GetID Json:"+e);
                }

            }
            else if(type.equals("request_update_loc"))
            {
                type="request_update_loc";

                json="1";
            }


// successfully finished
            result = Activity.RESULT_OK;




            publishResults(json, result, type);
        }

        private void publishResults(String json, int result,String type) {
            Intent intent = new Intent(NOTIFICATION);
            intent.putExtra(TYPE, type);
            intent.putExtra(DATA, json);
            intent.putExtra(RESULT, result);
            sendBroadcast(intent);
        }
    }

