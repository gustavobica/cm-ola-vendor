package com.gustavobica.www.olavendedor.Fragments.RequestFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.gustavobica.www.olavendedor.Adapters.BuyListAdapter;
import com.gustavobica.www.olavendedor.Main.MainActivity;
import com.gustavobica.www.olavendedor.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gustavo Bica on 04/04/2016.
 */
public class RequestListFragment extends Fragment {

    List<String> list=new ArrayList<String>();
    ListView lv;
    private ArrayAdapter<String> mAdapter;
    View rootView;

    TextView chat;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.chat_fragment, container, false);
        lv = (ListView) rootView.findViewById(R.id.chatList);
        //lv=(ListView) rootView.findViewById(R.id.listViewChat);
        //list=new String[]{"Sem Mensagens Novas"};
        list.add(0, "Sem Novos Pedidos!");
        //list[0]="Corneto morango";
        mAdapter = new ArrayAdapter<String>(rootView.getContext(), R.layout.chat_item, R.id.textViewChat, list);
        lv.setAdapter(mAdapter);
        ((MainActivity)getActivity()).getRequest("GetRequests");
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println(list.get(position));
                ((MainActivity) getActivity()).updateTextRequest(list.get(position), position);


            }
        });
        return rootView;
    }
    public void updateRequestView(List<String> list)
    {

        this.list=list;
        mAdapter= new ArrayAdapter<String>(rootView.getContext(),R.layout.chat_item,R.id.textViewChat,list);
        mAdapter.notifyDataSetChanged();
        lv.setAdapter(mAdapter);



    }
    public void checkoutRequest(int pos)
    {


    }


}

