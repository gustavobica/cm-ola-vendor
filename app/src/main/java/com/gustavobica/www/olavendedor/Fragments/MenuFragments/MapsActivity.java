package com.gustavobica.www.olavendedor.Fragments.MenuFragments;

import android.graphics.Color;
import android.location.LocationProvider;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Step;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.gustavobica.www.olavendedor.Main.MainActivity;
import com.gustavobica.www.olavendedor.R;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MapsActivity extends Fragment implements OnMapReadyCallback, com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private GoogleMap mMap;
    View rootView;
    protected GoogleApiClient mGoogleApiClient;
    protected static final String TAG = "MapsActivity";
    Location location;
    private static final long ONE_MIN = 1000 * 60;
    private static final long TWO_MIN = ONE_MIN * 2;
    private static final long FIVE_MIN = ONE_MIN * 5;
    private static final long POLLING_FREQ = 1000 * 30;
    private static final long FASTEST_UPDATE_FREQ = 1000 * 5;
    private static final float MIN_ACCURACY = 25.0f;
    private static final float MIN_LAST_READ_ACCURACY = 500.0f;

    private LocationRequest mLocationRequest;
    private Location mBestReading;
    View mapView=null;
    FragmentManager fm;
    SupportMapFragment myMapFragment;
    private static UiSettings mUiSettings;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    LatLng latLng;
    private LocationManager locationManager;
    Location mCurrentLocation;
    String mLastUpdateTime;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView=inflater.inflate(R.layout.activity_maps, container, false);
       //it automatically checks for null pointer or older play services version, getMap() is deprecated as mentioned in google docs.
        if (mMap == null)
            myMapFragment=((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
                    myMapFragment.getMapAsync(MapsActivity.this);
        locationManager = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);
        return rootView;
    }

    /*@Override
    public void onLocationChanged(Location location) {
        Toast.makeText(getActivity(), "location :" + location.getLatitude() + " , " + location.getLongitude(), Toast.LENGTH_SHORT).show();
        LatLng latlong = new LatLng(location.getLatitude(),
                location.getLongitude());
        System.out.println("AAAAAQQQQQRRR"+location.getLatitude());
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latlong));
        mMap.animateCamera(CameraUpdateFactory.zoomBy(18));
    }*/





    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
//        location.getLongitude();
  //      location.getLatitude();
        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);

        if(location!=null) {
            // Getting latitude of the current location
            double latitude = location.getLatitude();

            // Getting longitude of the current location
            double longitude = location.getLongitude();

            // Creating a LatLng object for the current location
            latLng = new LatLng(latitude, longitude);
            getEventsLocation(latitude,longitude);




            mMap.addMarker(new MarkerOptions().position(latLng).title("Start"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            ask_update_location();
        }else{
          /* latLng = new LatLng(location.getLatitude(), location.getLongitude());

        // Add a marker in Sydney and move the camera
          LatLng sydney = new LatLng(-34, 151);
         mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
         mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));*/
        }


        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mUiSettings = mMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(true);
        mUiSettings.setCompassEnabled(true);
        mUiSettings.setMyLocationButtonEnabled(true);



    }

public void getEventsLocation(double lat,double log)
{
    ((MainActivity) getActivity()).getCity(lat, log);
    ((MainActivity)getActivity()).getWeather(lat, log);

}
public void getPathToEvent(String address)
{
    System.out.println(latLng.latitude);
    //latLng = new LatLng(location.getLatitude(), location.getLongitude());
    mMap.clear();
    mMap.addMarker(new MarkerOptions().position(latLng).title("Start"));
    double latitude=0;
    double longitude=0;


    System.out.println(address);
    latitude = Double.parseDouble(address.split(",")[0]);
    longitude=Double.parseDouble(address.split(",")[1]);
    mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Evento"));
    System.out.println(latitude);
    if(!address.equals("None") && !address.equals("Indisponível") ) {
        String serverKey = "AIzaSyBYsZy07rg9NI6fJTbp5t7X8qRd2Bx3brs";
        LatLng origin = latLng;
        LatLng destination = new LatLng(latitude, longitude);
        GoogleDirection.withServerKey(serverKey)
                .from(latLng)
                .to(destination)
                .execute(new DirectionCallback() {

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        System.out.println("aqui");
                        Toast.makeText(getActivity(), "yip", Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        // Do something here
                        System.out.println("ALOOO");


                        if (direction.isOK()) {
                            System.out.println("ALOOO2");
                            List<Step> stepList = direction.getRouteList().get(0).getLegList().get(0).getStepList();
                            ArrayList<PolylineOptions> polylineOptionList = DirectionConverter.createTransitPolyline(getActivity(), stepList, 5, Color.RED, 3, Color.BLUE);
                            for (PolylineOptions polylineOption : polylineOptionList) {
                                mMap.addPolyline(polylineOption);

                            }

                            // Do something
                        } else {
                            // Do something
                        }
                        System.out.println(direction.isOK());
                        Toast.makeText(getActivity(), "Never mind", Toast.LENGTH_LONG);
                    }
                });
    }else{
        System.out.println("Não tem um endereço válido");
    }
}


@Override
public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    switch (requestCode) {
        case REQUEST_CODE_ASK_PERMISSIONS:
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission Granted
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mUiSettings = mMap.getUiSettings();
                mUiSettings.setZoomControlsEnabled(false);
                mUiSettings.setCompassEnabled(true);
                mUiSettings.setMyLocationButtonEnabled(true);
            } else {
                // Permission Denied
                Toast.makeText(getActivity(), "WRITE_CONTACTS Denied", Toast.LENGTH_SHORT)
                        .show();
            }
            break;
        default:
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
    @Override
    public void onResume() {
        super.onResume();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }
    @Override
    public void onPause() {
        super.onPause();

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    public Location getLocation()
    {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);

        System.out.println(location);
        return  location;
    }
    public void ask_update_location()
    {
        ((MainActivity)getActivity()).update_location_request();
    }


    @Override
    public void onConnected(Bundle dataBundle) {
        // Get first reading. Get additional location updates if necessary
        if (servicesAvailable()) {
            // Get best last location measurement meeting criteria
            mBestReading = bestLastKnownLocation(MIN_LAST_READ_ACCURACY, FIVE_MIN);

            if (null == mBestReading
                    || mBestReading.getAccuracy() > MIN_LAST_READ_ACCURACY
                    || mBestReading.getTime() < System.currentTimeMillis() - TWO_MIN) {
                System.out.println("AAAAAWWW");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

                // Schedule a runnable to unregister location listeners
                Executors.newScheduledThreadPool(1).schedule(new Runnable() {

                    @Override
                    public void run() {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MapsActivity.this);
                    }

                }, ONE_MIN, TimeUnit.MILLISECONDS);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private Location bestLastKnownLocation(float minAccuracy, long minTime) {
        Location bestResult = null;
        float bestAccuracy = Float.MAX_VALUE;
        long bestTime = Long.MIN_VALUE;

        // Get the best most recent location currently available
        Location mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mCurrentLocation != null) {
            float accuracy = mCurrentLocation.getAccuracy();
            long time = mCurrentLocation.getTime();

            if (accuracy < bestAccuracy) {
                bestResult = mCurrentLocation;
                bestAccuracy = accuracy;
                bestTime = time;
            }
        }

        // Return best reading or null
        if (bestAccuracy > minAccuracy || bestTime < minTime) {
            return null;
        }
        else {
            return bestResult;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private boolean servicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        }
        else {
            GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), 0).show();
            return false;
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        System.out.println("MUDEI");
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        updateLocation("http://gustavobica.com/CM/API.php?action=post_location&id=1&location=("
                + String.valueOf(mCurrentLocation.getLatitude()) + "," + String.valueOf(mCurrentLocation.getLongitude()) + ")");
    }


    private void updateLocation(String url){
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        StringBuilder builder = new StringBuilder();

        try {
            URL urLink = new URL(url);
            connection = (HttpURLConnection) urLink.openConnection();
            connection.connect();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }


}

