package com.gustavobica.www.olavendedor.Main;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;//a
import android.support.v7.app.AppCompatActivity;//a
import android.support.v7.widget.Toolbar;//a
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.gustavobica.www.olavendedor.Activities.LoginActivity;
import com.gustavobica.www.olavendedor.Activities.SaveSharedPreference;
import com.gustavobica.www.olavendedor.Fragments.MenuFragments.ButtonsFragment;
import com.gustavobica.www.olavendedor.Fragments.RequestFragments.RequestDetailFragment;
import com.gustavobica.www.olavendedor.Fragments.RequestFragments.RequestListFragment;
import com.gustavobica.www.olavendedor.Fragments.SellFragments.BuyListFragment;
import com.gustavobica.www.olavendedor.Fragments.SalesFragments.ChartsFragment;
import com.gustavobica.www.olavendedor.Fragments.SellFragments.Detail_IcreamFragment;
import com.gustavobica.www.olavendedor.Fragments.MenuFragments.EventsFragment;
import com.gustavobica.www.olavendedor.Fragments.SellFragments.IcreamListFragment;
import com.gustavobica.www.olavendedor.Fragments.MenuFragments.MapsActivity;
import com.gustavobica.www.olavendedor.Fragments.MenuFragments.WeatherFragment;
import com.gustavobica.www.olavendedor.Fragments.SalesFragments.WebViewFragment;
import com.gustavobica.www.olavendedor.Fragments.RequestFragments.chat_fragment;
import com.gustavobica.www.olavendedor.IntentService.GetService;
import com.gustavobica.www.olavendedor.IntentService.TaskSchedule;
import com.gustavobica.www.olavendedor.R;
import com.gustavobica.www.olavendedor.database.IcreamDatabase;
import com.gustavobica.www.olavendedor.parsers.EventsParser;
import com.gustavobica.www.olavendedor.parsers.WeatherParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{



    Location mLastLocation=null;
    ArrayList<String[]> list=new ArrayList<String[]>();
    FragmentTransaction transaction;
    private IcreamDatabase dbHelper;
    private double myLat;
    private double myLog;
    private String myCity=null;
    public String myUsername=null;
    public String myID;
    public int local=0;
    public LocationManager locationManager;
    public JSONObject request_json;
    public JSONArray request_jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(SaveSharedPreference.getUserName(MainActivity.this).length() == 0)
        {
            // call Login Activity
            Intent login=new Intent(this,LoginActivity.class);
            startActivity(login);
        }else {
            myUsername=SaveSharedPreference.getUserName(MainActivity.this);
            setContentView(R.layout.activity_main);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            dbHelper = new IcreamDatabase(this);

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();
            MapsActivity map = new MapsActivity();
            WeatherFragment weather = new WeatherFragment();
            ButtonsFragment buttons = new ButtonsFragment();
            EventsFragment events = new EventsFragment();
            // FragmentManager manager=getSupportFragmentManager();
            transaction = getSupportFragmentManager().beginTransaction();
            // manager.beginTransaction();
            transaction.add(R.id.MapFragment, map);
            transaction.add(R.id.WeatherFragment, weather);
            transaction.add(R.id.ButtonFragment, buttons);
            transaction.add(R.id.EventsFragment, events);
            transaction.commit();
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            GetID(myUsername);

        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    public void SendToMap(String address)
    {
        MapsActivity mapfragment = (MapsActivity)getSupportFragmentManager().findFragmentById(R.id.MapFragment);
        mapfragment.getPathToEvent(address);

    }
    public void sendIceToDetail(String name,String qty,String price)
    {
        if(local==1)
        {
            Detail_IcreamFragment detailfrag=( Detail_IcreamFragment)getSupportFragmentManager().findFragmentById(R.id.MapFragment);
            detailfrag.updateCharts(name);
        }else
        {
            BuyListFragment buyfrag=(BuyListFragment)getSupportFragmentManager().findFragmentById(R.id.MapFragment);
            buyfrag.updateBuyListView(name, qty, price);

        }

    }
    public void UpdateToSalesView()
    {
        ChartsFragment chartfrag=new ChartsFragment();
        Detail_IcreamFragment detailfrag=new Detail_IcreamFragment();
        IcreamListFragment icreamList=new IcreamListFragment();
        transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.weatherFrag, chartfrag);
        transaction.replace(R.id.MapFragment, detailfrag);
        transaction.replace(R.id.EventsFragment, icreamList);
        transaction.addToBackStack(null);
        transaction.commit();
        local=1;

    }
    public void UpdateToSellView()
    {
        WebViewFragment webfrag=new WebViewFragment();
        IcreamListFragment icreamList=new IcreamListFragment();
        BuyListFragment buyfrag=new BuyListFragment();
        transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.weatherFrag, webfrag);
        transaction.replace(R.id.MapFragment, buyfrag);
        transaction.replace(R.id.EventsFragment, icreamList);
        transaction.addToBackStack(null);
        transaction.commit();
        local=2;

    }
    public void UpdateToMenuView()
    {
        MapsActivity map = new MapsActivity();
        WeatherFragment weather = new WeatherFragment();
        ButtonsFragment buttons = new ButtonsFragment();
        EventsFragment events = new EventsFragment();
        // FragmentManager manager=getSupportFragmentManager();
        transaction = getSupportFragmentManager().beginTransaction();
        // manager.beginTransaction();
        transaction.replace(R.id.MapFragment, map);
        transaction.replace(R.id.WeatherFragment, weather);
        transaction.replace(R.id.ButtonFragment, buttons);
        transaction.replace(R.id.EventsFragment, events);
        transaction.addToBackStack(null);
        transaction.commit();
        local=0;


        getWeather(myLat, myLog);
        getEvents(myCity);

    }
    public void UpdateToRequestView()
    {
        local=3;
        chat_fragment chatfrag =new chat_fragment();
        RequestDetailFragment detailfrag=new RequestDetailFragment();
        RequestListFragment listFragment=new RequestListFragment();

        // FragmentManager manager=getSupportFragmentManager();
        transaction = getSupportFragmentManager().beginTransaction();
        // manager.beginTransaction();
        transaction.replace(R.id.MapFragment, detailfrag);
        transaction.replace(R.id.WeatherFragment, chatfrag);
        transaction.replace(R.id.EventsFragment, listFragment);
        transaction.addToBackStack(null);
        transaction.commit();


        // getEvents("Aveiro");

    }
    public void updateTextRequest(String request,int pos)
    {
        RequestDetailFragment detailfragment = (RequestDetailFragment) getSupportFragmentManager().findFragmentById(R.id.MapFragment);
        detailfragment.updateTextRequest(request, pos);

    }
    public void checkoutRequest(int pos)
    {
        RequestListFragment listRfragment = (RequestListFragment) getSupportFragmentManager().findFragmentById(R.id.MapFragment);
       listRfragment.checkoutRequest(pos);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camara) {
            UpdateToMenuView();
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

            UpdateToSalesView();
        } else if (id == R.id.nav_slideshow) {
            UpdateToSellView();
        } else if (id == R.id.nav_manage) {
            UpdateToRequestView();
        } else if (id == R.id.nav_share) {
            SaveSharedPreference.clearUserName(getBaseContext());
            Intent Login = new Intent(getBaseContext(), LoginActivity.class);
            logout();
            startActivity(Login);


        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void postStock(String username)
    {
        Intent intent = new Intent(this, GetService.class);

        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE, "PostStock");
        String json='{'+'"'+"Stock"+'"'+':'+'[';
        Cursor rs=dbHelper.getAllIcream();
        JSONObject obj=new JSONObject();
        JSONObject item=new JSONObject();
        JSONArray array=new JSONArray();
        try {


        if(rs.moveToFirst()) {
            int i=0;
            String NameIce;
            int Stock;
            int Price;

            do {
                NameIce=rs.getString(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_NAME));
                Stock=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_QTY));
                Price=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_PRICE));
                list.add(i,new String[]{NameIce,Integer.toString(Stock),Integer.toString(Price)});
                i++;
                item.put("name", NameIce);
                item.put("quant",Stock);
                array.put(item);
                item=new JSONObject();
                //item.remove("name");
                //item.remove("quant");
                json=json+'{'+'"'+"name"+'"'+':'+'"'+NameIce+'"'+','+'"'+"quant"+'"'+':'+Stock+'}';
            } while (rs.moveToNext());
            json=json+"]"+"}";
            obj.put("Stock", array);
            System.out.println(obj.toString());
        }
        }catch (JSONException e)
        {
            System.out.println("Post Stock:"+e);
        }
        IcreamListFragment icefragment = (IcreamListFragment)getSupportFragmentManager().findFragmentById(R.id.EventsFragment);
        icefragment.updateIceListView();

        //System.out.println(encodeURIComponent(JSON.stringify(obj.toString())));
        System.out.println(obj.toString());
        //json=json.replace(" ", "%20");
        //String r="\"\
        System.out.println("http://gustavobica.com/CM/API.php?action=post_stock&stock=" +URLEncoder.encode(obj.toString())+ "&username=" + myUsername);
        intent.putExtra(GetService.URL,
                "http://gustavobica.com/CM/API.php?action=post_stock&stock=" +URLEncoder.encode(obj.toString())+ "&username=" + myUsername);
        startService(intent);



    }

    public  void UpdateStock(String username)
    {
        IcreamListFragment icefragment = (IcreamListFragment)getSupportFragmentManager().findFragmentById(R.id.EventsFragment);
        icefragment.updateIceListView();
       postStock(username);

    }
    public void GetID(String username)
    {
        Intent intent = new Intent(this, GetService.class);

        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE, "GetID");
        intent.putExtra(GetService.URL,
                "http://gustavobica.com/CM/API.php?action=usernameToId&username=" + username);
        startService(intent);

    }
    public void getWeather(double lat,double log) {
        myLat=lat;
        myLog=log;
        Intent intent = new Intent(this, GetService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE, "weather");
        intent.putExtra(GetService.URL,
                "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + log + "&APPID=1101b7e5a7a26d14341a4d0698553e27");
        startService(intent);

    }
    public void update_location()
    {
        if (local==0)
        {
            MapsActivity mapfragment = (MapsActivity) getSupportFragmentManager().findFragmentById(R.id.MapFragment);
            post_location(mapfragment.getLocation());
        }
    }
    public void post_location(Location location)
    {
        double myLat=location.getLatitude();
        double myLog=location.getLongitude();
        Intent intent = new Intent(this, GetService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE, "PostLocation");
        intent.putExtra(GetService.URL,
                "http://gustavobica.com/CM/API.php?action=post_location&id=" + myID + "&lat=" + myLat + "&log=" + myLog);//"http://gustavobica.com/CM/API.php?action=post_location&id=" + myID +"&location="+"("+myLat+","+myLog+")");
        System.out.println("http://gustavobica.com/CM/API.php?action=post_location&id=" + myID + "&lat=" + myLat + "&log=" + myLog);
        startService(intent);
    }
    public void update_location_request()
    {
    PendingIntent pendingIntent;
    AlarmManager manager;
    Intent alarmIntent = new Intent(this, TaskSchedule.class);
    alarmIntent.putExtra("Type","location");
    alarmIntent.putExtra("Username", myID);


    pendingIntent = PendingIntent.getBroadcast(this, 123, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
    int interval = 10000;

    manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);


}
    public  void logout()
    {
        Intent intentstop = new Intent(this, TaskSchedule.class);
        PendingIntent senderstop = PendingIntent.getBroadcast(this,
                123456, intentstop, 0);
        AlarmManager alarmManagerstop = (AlarmManager) getSystemService(ALARM_SERVICE);

        alarmManagerstop.cancel(senderstop);

        Intent intentstop2 = new Intent(this, TaskSchedule.class);
        PendingIntent senderstop2 = PendingIntent.getBroadcast(this,
                12345, intentstop2, 0);
        AlarmManager alarmManagerstop2 = (AlarmManager) getSystemService(ALARM_SERVICE);

        alarmManagerstop2.cancel(senderstop2);

        Intent intentstop3 = new Intent(this, TaskSchedule.class);
        PendingIntent senderstop3 = PendingIntent.getBroadcast(this,
                123, intentstop3, 0);
        AlarmManager alarmManagerstop3 = (AlarmManager) getSystemService(ALARM_SERVICE);

        alarmManagerstop3.cancel(senderstop);

        Intent intent = new Intent(this, GetService.class);

        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE, "logout");
        intent.putExtra(GetService.URL,
                "http://gustavobica.com/CM/API.php?action=logout&username=" + myUsername);
        startService(intent);

    }
    public void getRequest(String type) {


        PendingIntent pendingIntent;
        AlarmManager manager;
        Intent alarmIntent = new Intent(this, TaskSchedule.class);
        alarmIntent.putExtra("Type", type);
        alarmIntent.putExtra("Username", myID);


        pendingIntent = PendingIntent.getBroadcast(this, 123456, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        int interval = 10000;

        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);




    }
    public void getChat(String type)
    {
        PendingIntent pendingIntent;
        AlarmManager manager;
        Intent alarmIntent = new Intent(this, TaskSchedule.class);
        alarmIntent.putExtra("Type",type);
        alarmIntent.putExtra("Username", myID);


        pendingIntent = PendingIntent.getBroadcast(this, 12345, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        int interval = 10000;

        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);

    }
    public void getEvents(String city)
    {
        Intent intent = new Intent(this, GetService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE, "events");
        intent.putExtra(GetService.URL,
                "http://api.eventful.com/json/events/search?app_key=vQcJ6N36NQM3dd8J&location=" + city + "");
        startService(intent);

    }

    public void getCity(double lat, double log) {
        Intent intent = new Intent(this, GetService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE, "findLocation");
        intent.putExtra(GetService.URL,
                "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + log);
        startService(intent);

    }
    public void clear_request(int pos)
    {
        if(pos==0)
        {

        }else{

        }
        Intent intent = new Intent(this, GetService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE, "clearRequest");
        intent.putExtra(GetService.URL,
                "http://gustavobica.com/CM/API.php?action=clear_request&id="+myID+"&pos="+pos);
        startService(intent);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String type = bundle.getString(GetService.TYPE);
                int resultCode = bundle.getInt(GetService.RESULT);
                String json=bundle.getString(GetService.DATA);


                JSONObject jsonObj=null;
                JSONArray jsonArray=null;
                if (resultCode == RESULT_OK) {


                    if (type.equals("weather") && local==0) {
                        WeatherParser a=new WeatherParser(json);
                        WeatherFragment weatherfragment = (WeatherFragment) getSupportFragmentManager().findFragmentById(R.id.WeatherFragment);
                        weatherfragment.updateTextView(a.GetTemp_max(),a.GetTemp_min(), a.getDescription(), a.GetHumidity(),a.GetTemp(),a.getIcon());



                    } else if (type.equals("events") && local==0) {
                        EventsParser b= new EventsParser(json);
                        list=b.getJson();
                        EventsFragment eventfragment = (EventsFragment)getSupportFragmentManager().findFragmentById(R.id.EventsFragment);
                        eventfragment.updateListView(list);



                    }else if (type.equals("getStock")) {

                        try {
                            jsonObj = new JSONObject(json);
                            jsonArray = jsonObj.getJSONArray("Stock");
                            System.out.println(dbHelper.size());
                            dbHelper.deleteAllIcreams();
                            System.out.println(dbHelper.size());
                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonObj = jsonArray.getJSONObject(i);
                                dbHelper.insertIcream(jsonObj.getString("name"), jsonObj.getInt("quant"), 0);

                            }
                            System.out.println(dbHelper.size());


                        } catch (JSONException e) {
                            System.out.println("getStock:" + e);
                        }


                    }else if (type.equals("findLocation")) {

                            //System.out.println(json);
                        try {
                            JSONObject ObjectCity = new JSONObject(json);
                            //System.out.println(ObjectCity);
                            JSONArray ArrayCity = ObjectCity.getJSONArray("results");
                            //System.out.println(ArrayCity);
                            ObjectCity=ArrayCity.optJSONObject(0);
                            //System.out.println(ObjectCity);

                            ArrayCity = ObjectCity.getJSONArray("address_components");
                            //System.out.println(ArrayCity);

                            ObjectCity = ArrayCity.getJSONObject(2);
                            //System.out.println(ObjectCity);
                            //System.out.println(ObjectCity.getString("long_name"));
                            getEvents(ObjectCity.getString("long_name"));




                        } catch (JSONException e) {
                            System.out.println("findLocation:" + e);
                        }


                    }else if (type.equals("GetID")) {

                        myID=json;

                    }else if (type.equals("GetChat") && local==3) {


                        List<String> list=new ArrayList<String>();

                        try {
                            json=json.replace("\\\"","'");

                            System.out.println(json);
                            JSONArray chat = new JSONArray(json.substring(1,json.length()-1));
                            System.out.println(chat);

                                for (int i = 0; i < chat.length(); i++) {
                                    list.add(i,chat.getJSONObject(i).getString("Name") + " diz:" + chat.getJSONObject(i).getString("Phrase"));
                                }




                        } catch (JSONException e) {

                            System.out.println("GetChat:" + e);
                            list.add(0, "Sem Mensagens Novas");
                        }

                        chat_fragment chatfrag = (chat_fragment)getSupportFragmentManager().findFragmentById(R.id.WeatherFragment);
                        chatfrag.updateChatView(list);


                    }
                    else if (type.equals("GetRequests") && local==3) {


                        List<String> list=new ArrayList<String>();

                        try {



                            JSONObject request = new JSONObject(json);
                            JSONObject temp= request;
                            request_json=temp;
                            if(request.length()>1) {
                                JSONArray requestArray= request.getJSONArray("pedido");
                                request = requestArray.getJSONObject(0);

                                list.add(0, "Pedido: " + request.getString("name") + "\n" + "Quantidade: " + request.getString("quant") + "\n" + "ID: " + request.getString("id"));
                                request=temp;


                                for (int i = 1; i < request.length(); i++) {
                                    temp=request;
                                    temp= request.getJSONObject(Integer.toString(i-1));


                                    //temp = requestArray.getJSONObject(0);

                                    list.add(i,"Pedido: "+temp.getString("name")+"\n"+"Quantidade: "+temp.getString("quant")+"\n"+"ID: "+temp.getString("id"));
                                }
                            }
                            else{
                                JSONArray requestArray= request.getJSONArray("pedido");
                                request = requestArray.getJSONObject(0);

                                list.add(0,"Pedido: "+request.getString("name")+"\n"+"Quantidade: "+request.getString("quant")+"\n"+"ID: "+request.getString("id"));
                            }



                        } catch (JSONException e) {

                            System.out.println("GetChat:" + e);
                            list.add(0, "Sem Novos pedidos!");
                        }

                        RequestListFragment requestfrag = (RequestListFragment)getSupportFragmentManager().findFragmentById(R.id.EventsFragment);
                        requestfrag.updateRequestView(list);


                    }else if (type.equals("request_update_loc")) {


                            update_location();

                    }else{





                    }

                } else {

                }

            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(receiver, new IntentFilter(GetService.NOTIFICATION));

    }
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);

    }








}
