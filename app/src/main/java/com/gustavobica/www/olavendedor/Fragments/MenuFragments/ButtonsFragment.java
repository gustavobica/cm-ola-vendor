package com.gustavobica.www.olavendedor.Fragments.MenuFragments;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;

import com.gustavobica.www.olavendedor.Main.MainActivity;
import com.gustavobica.www.olavendedor.R;


public class ButtonsFragment extends Fragment {
    Button Vendas;
    Button Vender;
    Button Pedidos;
    Button Menu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.content_main, container, false);

        Vendas = (Button) rootView.findViewById(R.id.buttonVendas);
        Vender = (Button) rootView.findViewById(R.id.buttonVender);
        Pedidos = (Button) rootView.findViewById(R.id.buttonPedidos);
        Menu = (Button) rootView.findViewById(R.id.buttonMenu);


        Vendas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).UpdateToSalesView();

            }
        });
        Vender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity)getActivity()).UpdateToSellView();
            }
        });
        Pedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).UpdateToRequestView();

            }
        });
        Menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).UpdateToMenuView();

            }
        });


        return rootView;
    }
}