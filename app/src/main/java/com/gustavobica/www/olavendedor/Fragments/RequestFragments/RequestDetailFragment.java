package com.gustavobica.www.olavendedor.Fragments.RequestFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gustavobica.www.olavendedor.Adapters.BuyListAdapter;
import com.gustavobica.www.olavendedor.R;

import java.util.ArrayList;

/**
 * Created by Gustavo Bica on 04/04/2016.
 */
public class RequestDetailFragment extends Fragment {

    String Request= "Clique num pedido!";
    TextView textRequest;
    Button checkout;
    private ArrayAdapter<String[]> mAdapter;
    View rootView;
    int pos;
    TextView chat;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.detail_request, container, false);
        checkout=(Button) rootView.findViewById(R.id.buttonCheckout);
        textRequest=(TextView) rootView.findViewById(R.id.textViewRequest);
        textRequest.setText(Request);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Request.equals("Clique num pedido!"))
                {
                    String Request1 = Request.split("\n")[0];
                    String Request2 = Request.split("\n")[1];
                    String Request3 = Request.split("\n")[2];
                    System.out.println(Request1);
                    System.out.println(Request2);
                    System.out.println(Request3);
                    //longitude=Double.parseDouble(address.split(",")[1]);
                }
            }
        });
        return rootView;
    }
    public void updateTextRequest(String request,int pos)
    {
        Request=request;
        this.pos=pos;
        textRequest.setText(Request);
    }
    public void checkoutRequest(String request,int pos)
    {
        Request=request;
        this.pos=pos;
        textRequest.setText(Request);
    }

}

