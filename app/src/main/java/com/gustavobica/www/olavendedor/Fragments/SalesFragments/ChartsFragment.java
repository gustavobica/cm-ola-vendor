package com.gustavobica.www.olavendedor.Fragments.SalesFragments;

import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.maps.SupportMapFragment;
import com.gustavobica.www.olavendedor.R;
import com.gustavobica.www.olavendedor.database.IcreamDatabase;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Gustavo Bica on 02/04/2016.
 */
public class ChartsFragment extends Fragment {
    View rootView;
    PieChart piechartTotals;
    PieChart piechart;
    IcreamDatabase dbhelper;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView=inflater.inflate(R.layout.charts_fragment, container, false);
        //it automatically checks for null pointer or older play services version, getMap() is deprecated as mentioned in google docs.
        // in this example, a LineChart is initialized from xml
        piechartTotals = (PieChart) rootView.findViewById(R.id.linechart);
        piechart = (PieChart) rootView.findViewById(R.id.piechart);



        dbhelper = new IcreamDatabase(getActivity().getBaseContext());
        Cursor rs=dbhelper.getAllIcream();
        if(rs.moveToFirst()) {
            int i=0;
            String NameIce;
            int Stock;
            int Price;
            int Sales;
            ArrayList<Entry> entriesProfit = new ArrayList<>();
            ArrayList<Entry> entriesSales = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            do {
                NameIce=rs.getString(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_NAME));
                System.out.println(NameIce);
                Stock=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_QTY));
                Price=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_PRICE));
                Sales=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_SALES));
                System.out.println("Charts" + Sales);
                entriesProfit.add(new Entry(Price * Sales, i));
                entriesSales.add(new Entry(Sales,i));
                labels.add(NameIce);
               // dbhelper.madeSale(NameIce);
                i++;
            } while (rs.moveToNext());
            PieDataSet dataSetSales = new PieDataSet(entriesSales,"# of Sales");
           PieDataSet datasetProfit = new PieDataSet(entriesProfit, "# of Euros");
            dataSetSales.setColors(ColorTemplate.COLORFUL_COLORS);
            datasetProfit.setColors(ColorTemplate.COLORFUL_COLORS);
            PieData data = new PieData(labels, dataSetSales);
            PieData data1= new PieData(labels,datasetProfit);
            piechartTotals.setData(data); // set the data and list of lables into chart
            piechartTotals.setDescription("Vendas Totais");
            piechart.setData(data1);
            piechart.setDescription("Lucro Total");
        }

        return rootView;
    }
    public void updateCharts(String Name)
    {
        Cursor rs=dbhelper.getAllIcream();
        if(rs.moveToFirst()) {
            int i=0;
            String NameIce;
            int Stock;
            int Price;
            int Sales;
            ArrayList<Entry> entriesProfit = new ArrayList<>();
            ArrayList<Entry> entriesSales = new ArrayList<>();
            ArrayList<String> labels = new ArrayList<String>();

            do {
                NameIce=rs.getString(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_NAME));
                System.out.println(NameIce);
                Stock=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_QTY));
                Price=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_PRICE));
                Sales=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_SALES));
                System.out.println("Charts" + Sales);
                entriesProfit.add(new Entry(Price * Sales, i));
                entriesSales.add(new Entry(Sales,i));
                labels.add(NameIce);
                // dbhelper.madeSale(NameIce);
                i++;
            } while (rs.moveToNext());
            PieDataSet dataSetSales = new PieDataSet(entriesSales,"# of Sales");
            PieDataSet datasetProfit = new PieDataSet(entriesProfit, "# of Euros");
            PieData data = new PieData(labels, dataSetSales);
            PieData data1= new PieData(labels,datasetProfit);
            piechartTotals.setData(data); // set the data and list of lables into chart
            piechartTotals.setDescription("Vendas Totais");
            piechart.setData(data1);
            piechart.setDescription("Lucro Total");
        }
    }
}
