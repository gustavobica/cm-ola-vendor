package com.gustavobica.www.olavendedor.Fragments.MenuFragments;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.SupportMapFragment;
import com.gustavobica.www.olavendedor.R;

import java.io.InputStream;
import java.text.DecimalFormat;

/**
 * Created by Gustavo Bica on 31/03/2016.
 */
public class WeatherFragment extends Fragment {

    TextView Max;
    TextView Min;
    TextView Description;
    TextView humidity;
    TextView Temp;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView=inflater.inflate(R.layout.activity_weather, container, false);
        Max= (TextView) rootView.findViewById(R.id.textMaxTemp);
        Min= (TextView) rootView.findViewById(R.id.textMinTemp);
        Description= (TextView) rootView.findViewById(R.id.textDescription);
        humidity= (TextView) rootView.findViewById(R.id.textHumidity);
        Temp= (TextView) rootView.findViewById(R.id.textTemp);
        // show The Image in a ImageView


        return rootView;
    }
    public void updateTextView(String max,String min,String description,String humidity,String temp,String icon)
    {
        DecimalFormat df = new DecimalFormat("#.00");
        Max.setText("Max Temp:"+max);
        Min.setText("Min Temp:"+min);
        Description.setText(description);
        this.humidity.setText("Humidity:" + humidity);
        Temp.setText("Temp:" + temp);
        new DownloadImageTask((ImageView) getActivity().findViewById(R.id.imageViewWeather))
                .execute("http://openweathermap.org/img/w/"+icon+".png");

    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageDrawable(null);
            bmImage.setImageBitmap(result);
        }
    }
}
