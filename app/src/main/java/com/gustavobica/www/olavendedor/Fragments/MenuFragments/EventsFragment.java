package com.gustavobica.www.olavendedor.Fragments.MenuFragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.gustavobica.www.olavendedor.Adapters.EventsAdapter;
import com.gustavobica.www.olavendedor.Main.MainActivity;
import com.gustavobica.www.olavendedor.R;

import java.util.ArrayList;

/**
 * Created by Gustavo Bica on 01/04/2016.
 */
public class EventsFragment extends Fragment {

    ArrayList<String[]> list=new ArrayList<String[]>();
    ListView lv;
    private EventsAdapter mAdapter;
    View rootView;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.eventsfragment, container, false);
        lv=(ListView) rootView.findViewById(R.id.listViewEvents);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        ((MainActivity)getActivity()).SendToMap(list.get(position)[5]);
            }
        });
        return rootView;
    }
    public void updateListView(ArrayList<String[]> list)
    {
        this.list=list;
        mAdapter = new EventsAdapter(rootView.getContext(),list);
        lv.setAdapter(mAdapter);

    }
}