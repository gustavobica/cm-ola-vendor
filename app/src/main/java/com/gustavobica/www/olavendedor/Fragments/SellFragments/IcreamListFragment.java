package com.gustavobica.www.olavendedor.Fragments.SellFragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.gustavobica.www.olavendedor.Adapters.EventsAdapter;
import com.gustavobica.www.olavendedor.Adapters.IcreamAdapter;
import com.gustavobica.www.olavendedor.Main.MainActivity;
import com.gustavobica.www.olavendedor.R;
import com.gustavobica.www.olavendedor.database.IcreamDatabase;

import java.util.ArrayList;

/**
 * Created by Gustavo Bica on 03/04/2016.
 */
public class IcreamListFragment  extends Fragment {

    ArrayList<String[]> list=new ArrayList<String[]>();
    ListView lv;
    private IcreamAdapter mAdapter;
    View rootView;
    IcreamDatabase dbHelper;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.eventsfragment, container, false);
        lv=(ListView) rootView.findViewById(R.id.listViewEvents);
        dbHelper = new IcreamDatabase(getActivity());
        updateIceListView();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!list.get(position)[1].equals("0"))
                    ((MainActivity) getActivity()).sendIceToDetail(list.get(position)[0], "1", "1");
                else {
                    Toast.makeText(getActivity().getBaseContext(), "Não tem gelados suficientes!", Toast.LENGTH_LONG).show();
                }

            }
        });
        return rootView;
    }
    public void updateIceListView()
    {
        Cursor rs=dbHelper.getAllIcream();
        if(rs.moveToFirst()) {
            int i=0;
            String NameIce;
            int Stock;
            int Price;

            do {
                NameIce=rs.getString(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_NAME));
                System.out.println(NameIce);
                Stock=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_QTY));
                Price=rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_PRICE));
                System.out.println(rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_SALES)));
                list.add(i,new String[]{NameIce,Integer.toString(Stock),Integer.toString(Price)});
                i++;
            } while (rs.moveToNext());

        }

        mAdapter = new IcreamAdapter(rootView.getContext(),list);
        lv.setAdapter(mAdapter);

    }
}
