package com.gustavobica.www.olavendedor.Fragments.RequestFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gustavobica.www.olavendedor.Adapters.BuyListAdapter;
import com.gustavobica.www.olavendedor.Adapters.EventsAdapter;
import com.gustavobica.www.olavendedor.Main.MainActivity;
import com.gustavobica.www.olavendedor.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Gustavo Bica on 04/04/2016.
 */
public class chat_fragment extends Fragment {

    List<String> list = new ArrayList<String>();
    //String [] list=null;
    ListView lv;
    private ArrayAdapter<String> mAdapter;
    View rootView;

    TextView chat;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.chat_fragment, container, false);
        lv = (ListView) rootView.findViewById(R.id.chatList);
        //lv=(ListView) rootView.findViewById(R.id.listViewChat);
        //list=new String[]{"Sem Mensagens Novas"};
        list.add(0, "Sem Mensagens Novas");
        //list[0]="Corneto morango";
        mAdapter = new ArrayAdapter<String>(rootView.getContext(), R.layout.chat_item, R.id.textViewChat, list);
        lv.setAdapter(mAdapter);
        ((MainActivity) getActivity()).getChat("GetChat");

        return rootView;
    }

    public void updateChatView(List<String> list) {

        this.list = list;
        Collections.reverse(this.list);
        mAdapter = new ArrayAdapter<String>(rootView.getContext(), R.layout.chat_item, R.id.textViewChat, list);
        mAdapter.notifyDataSetChanged();
        lv.setAdapter(mAdapter);


    }
}

