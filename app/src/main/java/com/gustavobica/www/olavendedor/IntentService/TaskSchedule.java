package com.gustavobica.www.olavendedor.IntentService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.gustavobica.www.olavendedor.Fragments.MenuFragments.MapsActivity;
import com.gustavobica.www.olavendedor.Main.MainActivity;

/**
 * Created by Gustavo Bica on 04/04/2016.
 */
public class TaskSchedule extends BroadcastReceiver {
    @Override
    public void onReceive(Context arg0, Intent arg1) {
        // For our recurring task, we'll just display a message

        if(arg1.getStringExtra("Type").equals("GetChat"))
        {
            getChat(arg0, arg1.getStringExtra("Username"),arg1.getStringExtra("Type"));

        }else if(arg1.getStringExtra("Type").equals("PostLocation"))
        {
            postLocation(arg0, arg1.getStringExtra("Username"),arg1.getStringExtra("Type"));


        }else if(arg1.getStringExtra("Type").equals("GetRequests"))
        {
            getRequest(arg0, arg1.getStringExtra("Username"),arg1.getStringExtra("Type"));

        }else{
            update_location(arg0, arg1.getStringExtra("Username"),"request_update_loc");
        }


    }
    public void postLocation(Context cx,String Username,String Type)
    {
        Intent intent = new Intent(cx, GetService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE,Type);
        intent.putExtra(GetService.URL,
                "http://gustavobica.com/CM/API.php?action=post_location&username=" +Username);
        cx.startService(intent);

    }
    public void update_location(Context cx,String Username,String Type)
    {
        Intent intent = new Intent(cx, GetService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE,Type);
        intent.putExtra(GetService.URL,
                "http://gustavobica.com/CM/API.php?action=post_location&username=" +Username);
        cx.startService(intent);
    }
    public void getRequest(Context cx,String id,String type)
    {
        Intent intent = new Intent(cx, GetService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE,type);
        intent.putExtra(GetService.URL,
                "http://gustavobica.com/CM/API.php?action=get_request&id=" +id);
        cx.startService(intent);

    }
    public void getChat(Context cx,String id,String Type)
    {
        Intent intent = new Intent(cx, GetService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(GetService.TYPE,Type);
        intent.putExtra(GetService.URL,
                "http://gustavobica.com/CM/API.php?action=get_chat&id=" +id);
        cx.startService(intent);
    }
}
