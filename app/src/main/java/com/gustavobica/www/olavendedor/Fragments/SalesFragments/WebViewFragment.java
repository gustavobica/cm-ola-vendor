package com.gustavobica.www.olavendedor.Fragments.SalesFragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ListView;

import com.gustavobica.www.olavendedor.Adapters.IcreamAdapter;
import com.gustavobica.www.olavendedor.Main.MainActivity;
import com.gustavobica.www.olavendedor.R;
import com.gustavobica.www.olavendedor.database.IcreamDatabase;

import java.util.ArrayList;

/**
 * Created by Gustavo Bica on 04/04/2016.
 */
public class WebViewFragment  extends Fragment {


    View rootView;

    WebView mWebView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.web_fragment, container, false);
        mWebView = (WebView) rootView.findViewById(R.id.my_webview);

        String url = "http://www.ola.pt/";
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        // probably a good idea to check it's not null, to avoid these situations:
        if(mWebView != null){
            mWebView.loadUrl(url);
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });
        }
        return rootView;
    }

}
