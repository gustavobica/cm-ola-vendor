package com.gustavobica.www.olavendedor.Fragments.SellFragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.data.Entry;
import com.gustavobica.www.olavendedor.Adapters.BuyListAdapter;
import com.gustavobica.www.olavendedor.Adapters.IcreamAdapter;
import com.gustavobica.www.olavendedor.Main.MainActivity;
import com.gustavobica.www.olavendedor.R;
import com.gustavobica.www.olavendedor.database.IcreamDatabase;

import java.util.ArrayList;

/**
 * Created by Gustavo Bica on 04/04/2016.
 */
public class BuyListFragment extends Fragment {

    ArrayList<String[]> list=new ArrayList<String[]>();
    ListView lv;
    private BuyListAdapter mAdapter;
    View rootView;
    Button confirmar;
    Button cancel;
    TextView totalprice;
    int start=0;
    IcreamDatabase dbhelper;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.buy_list_fragment, container, false);
        lv=(ListView) rootView.findViewById(R.id.listViewBuy);
        confirmar = (Button) rootView.findViewById(R.id.buttonBuyYes);
        confirmar.setVisibility(View.INVISIBLE);

        cancel = (Button) rootView.findViewById(R.id.buttonBuyNo);
        cancel.setVisibility(View.INVISIBLE);
        totalprice = (TextView) rootView.findViewById(R.id.textTotalPrice);

        list.add(0,new String[]{"------","-","-"});
        mAdapter= new BuyListAdapter(rootView.getContext(),list);
        lv.setAdapter(mAdapter);
        dbhelper =new IcreamDatabase(getActivity().getBaseContext());
        //dbHelper = new IcreamDatabase(getActivity());
        //Cursor rs=dbHelper.getAllIcream();

       /* lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ((MainActivity) getActivity()).sendIceToDetail(list.get(position)[0]);
            }
        });*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.clear();
                list.add(0, new String[]{"------", "-", "-"});
                mAdapter = new BuyListAdapter(rootView.getContext(),list);
                lv.setAdapter(mAdapter);
            }
        });
        confirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String NameIce;
                int Stock;
                int Price;
                int Sales;
                for (int i = 0; i < list.size(); i++) {

                    dbhelper.madeSale(list.get(i)[0]);
                    /*rs.moveToFirst();
                    NameIce = rs.getString(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_NAME));
                    if (list.get(i)[0].equals(NameIce)) {
                        System.out.println("by" + NameIce);
                        Stock = rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_QTY));
                        Price = rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_PRICE));
                        Sales = rs.getInt(rs.getColumnIndex(IcreamDatabase.CARD_COLUMN_SALES));
                        dbhelper.updateIcreamByName(NameIce, Stock - 1, Sales + 1);
                    *///}


                }
                ((MainActivity) getActivity()).postStock(((MainActivity) getActivity()).myUsername);
                list.clear();
                list.add(0, new String[]{"------", "-", "-"});
                mAdapter = new BuyListAdapter(rootView.getContext(),list);
                lv.setAdapter(mAdapter);
            }
        });
        return rootView;
    }
    public void updateBuyListView(String name,String qty,String price)
    {
        confirmar.setVisibility(View.VISIBLE);
        cancel.setVisibility(View.VISIBLE);
        if(list.size()==1 && start==0)
        {
            this.list.remove(0);
        }
        start=1;
        list.add(list.size(),new String[]{name,qty,price+"€"});
        mAdapter = new BuyListAdapter(rootView.getContext(),list);
        lv.setAdapter(mAdapter);
        int total=0;
        String temp;
        for(int i=0;i<list.size();i++)
        {
            System.out.println(list.get(i)[2]);
            temp=list.get(i)[2].split("€")[0];

            total=total+Integer.valueOf(temp);
            //total=+total;
        }
        totalprice.setText(Integer.toString(total)+"€");

    }


}
