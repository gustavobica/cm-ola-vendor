package com.gustavobica.www.olavendedor.database;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by Gustavo Bica on 02/04/2016.
 */
public class IcreamDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_IceCream_details = "IceCream_detail.db";
    public static final String DATABASE_ICEcream = "Icecreams.db";
    private static final int DATABASE_VERSION = 1;

    public static final String CARD_TABLE_NAME = "IceCream";
    public static final String CARD_COLUMN_ID = "_id";
    public static final String CARD_COLUMN_NAME = "name";
    public static final String CARD_COLUMN_QTY = "qty";
    public static final String CARD_COLUMN_SALES = "sales";
    public static final String CARD_COLUMN_PRICE = "price";
/*
    public static final String CARD_COLUMN_PROFILEID = "profileId";
    /*public static final String CARD_COLUMN_COMPANY = "company";
    public static final String CARD_COLUMN_ROLE = "role";   // cargo

    public static final String CARD_COLUMN_EMAIL = "email";
    public static final String CARD_COLUMN_PHONE = "phone";*/

    //public static final String CARD_COLUMN_IMAGE = "image";
    /*public static final String CARD_COLUMN_ADDRESS = "address";
    public static final String CARD_COLUMN_LINKEDIN = "linkedin";
    public static final String CARD_COLUMN_FACEBOOK = "facebook";
    public static final String CARD_COLUMN_TWITTER = "twitter";
    public static final String CARD_COLUMN_GPLUS = "gplus";
 */
    public IcreamDatabase(Context context) {
        super(context, DATABASE_IceCream_details, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + CARD_TABLE_NAME + "(" +
                        CARD_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        CARD_COLUMN_NAME + " TEXT, " +
                        CARD_COLUMN_QTY + " INTEGER, " +
                        CARD_COLUMN_PRICE + " INTEGER, " +
                        CARD_COLUMN_SALES + " INTEGER )"
                /*CARD_COLUMN_IMAGE + " TEXT ) " +
//                        CARD_COLUMN_ADDRESS + " TEXT, " +
                CARD_COLUMN_EMAIL + " TEXT, " +
                CARD_COLUMN_PHONE + " TEXT )"// +
//                        CARD_COLUMN_LINKEDIN + " TEXT, " +
//                        CARD_COLUMN_FACEBOOK + " TEXT, " +
//                        CARD_COLUMN_TWITTER + " TEXT, " +
//                        CARD_COLUMN_GPLUS + " TEXT)"*/
        );
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CARD_TABLE_NAME);
        onCreate(db);
    }
    public boolean insertIcream(String name,int quant ,int sales) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(CARD_COLUMN_NAME, name);
        contentValues.put(CARD_COLUMN_QTY, quant);
        contentValues.put(CARD_COLUMN_SALES, sales);
        contentValues.put(CARD_COLUMN_PRICE, 1);

        db.insert(CARD_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean updateIcream(Integer id,String name,Integer qty,Integer sales) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(CARD_COLUMN_NAME, name);
        contentValues.put(CARD_COLUMN_QTY, qty);
        contentValues.put(CARD_COLUMN_SALES, sales);


        db.update(CARD_TABLE_NAME, contentValues, CARD_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});
        return true;
    }
    public boolean updateIcreamByName(String name,Integer qty,Integer sales) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(CARD_COLUMN_NAME, name);
        contentValues.put(CARD_COLUMN_QTY, qty);
        contentValues.put(CARD_COLUMN_SALES, sales);


        db.update(CARD_TABLE_NAME, contentValues, CARD_COLUMN_NAME + " = ? ", new String[]{name});
        return true;
    }
    public boolean madeSale(Integer id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + CARD_TABLE_NAME + " WHERE " +
                CARD_COLUMN_ID + "=?", new String[]{Integer.toString(id)});
        int sale=res.getInt(res.getColumnIndex(IcreamDatabase.CARD_COLUMN_SALES));
        int qty=res.getInt(res.getColumnIndex(IcreamDatabase.CARD_COLUMN_QTY));
        ContentValues contentValues=new ContentValues();
        contentValues.put(CARD_COLUMN_SALES,sale+1);
        contentValues.put(CARD_COLUMN_QTY,qty-1);
        db.update(CARD_TABLE_NAME, contentValues, CARD_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});
        return true;
    }
    public boolean madeSale(String name)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + CARD_TABLE_NAME + " WHERE " +
                CARD_COLUMN_NAME + "=?", new String[]{name});
        res.moveToFirst();
        int sale=getSales(name);
        int qty=res.getInt(res.getColumnIndex(IcreamDatabase.CARD_COLUMN_QTY));
        ContentValues contentValues=new ContentValues();
        contentValues.put(CARD_COLUMN_SALES,sale+1);
        contentValues.put(CARD_COLUMN_QTY,qty-1);
        db.update(CARD_TABLE_NAME, contentValues, CARD_COLUMN_NAME + " = ? ", new String[]{name});
        return true;
    }
    public boolean ReStock(Integer id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + CARD_TABLE_NAME + " WHERE " +
                CARD_COLUMN_ID + "=?", new String[]{Integer.toString(id)});
        int qty=res.getInt(res.getColumnIndex(IcreamDatabase.CARD_COLUMN_QTY));
        ContentValues contentValues=new ContentValues();
        contentValues.put(CARD_COLUMN_QTY,qty+1);
        db.update(CARD_TABLE_NAME, contentValues, CARD_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});
        return true;

    }
    public boolean ReStock(String name)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + CARD_TABLE_NAME + " WHERE " +
                CARD_COLUMN_NAME + "=?", new String[]{name});
        res.moveToFirst();
        int qty=res.getInt(res.getColumnIndex(IcreamDatabase.CARD_COLUMN_QTY));
        ContentValues contentValues=new ContentValues();
        contentValues.put(CARD_COLUMN_QTY,qty+1);
        db.update(CARD_TABLE_NAME, contentValues, CARD_COLUMN_NAME + " = ? ", new String[]{name});
        return true;

    }

    public Cursor getIcream(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery( "SELECT * FROM " + CARD_TABLE_NAME + " WHERE " +
                CARD_COLUMN_ID + "=?", new String[] { Integer.toString(id) } );
        return res;
    }
    public Cursor getIcream(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery( "SELECT * FROM " + CARD_TABLE_NAME + " WHERE " +
                CARD_COLUMN_NAME + "=?", new String[] {name} );
        return res;
    }
    public int getSales(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        System.out.println(name);
        Cursor res = db.rawQuery( "SELECT * FROM " + CARD_TABLE_NAME + " WHERE " +
                CARD_COLUMN_NAME + "=?", new String[]{name} );
        res.moveToFirst();
        System.out.println(res.getCount());
        System.out.println(res.getString(res.getColumnIndex(IcreamDatabase.CARD_COLUMN_NAME)));
        return res.getInt(res.getColumnIndex(IcreamDatabase.CARD_COLUMN_SALES));
    }

    public Cursor getAllIcream() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery( "SELECT * FROM " + CARD_TABLE_NAME, null );
        return res;
    }

    public Integer deleteIcream (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(CARD_TABLE_NAME,
                CARD_COLUMN_ID + " = ? ",
                new String[] { Integer.toString(id) });
    }
    public Integer deleteAllIcreams () {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("DELETE FROM " + CARD_TABLE_NAME, null);

        int i=res.getCount();
        for(int j=1;j<i;j++)
        {
            db.delete(CARD_TABLE_NAME,
                    CARD_COLUMN_ID + " = ? ",
                    new String[] { Integer.toString(j) });
            if(j==i)
            {
                return db.delete(CARD_TABLE_NAME,
                        CARD_COLUMN_ID + " = ? ",
                        new String[] { Integer.toString(j) });
            }

        }
        return -1;
    }
    public long size()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        long cnt  = DatabaseUtils.queryNumEntries(db, CARD_TABLE_NAME);
        db.close();
        return cnt;
    }


}



