package com.gustavobica.www.olavendedor.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gustavobica.www.olavendedor.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by Gustavo Bica on 03/04/2016.
 */
public class IcreamAdapter extends BaseAdapter {



    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;
    private Typeface type;
    private Typeface type_bold;
    private ArrayList<String> mData = new ArrayList<String>();
    private ArrayList<String[]> list = new ArrayList<String[]>();
    private TreeSet<Integer> sectionHeader = new TreeSet<Integer>();
    private final int WHITE=0xffffffff;
    private Context context;
    private LayoutInflater mInflater;

    public IcreamAdapter (Context context,ArrayList<String[]> list) {
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list=list;
        this.context=context;

    }
    public IcreamAdapter (Context context, Typeface font,Typeface bold,ArrayList<String[]> list) {
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        type=font;
        type_bold=bold;
        this.list=list;
    }
    public void addItem(final String item) {
        mData.add(item);
        notifyDataSetChanged();
    }
    public void addItemArrayList(final ArrayList<String[]> item) {
        list=item;
        notifyDataSetChanged();
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public String getItem(int position) {
        String rturn;

        rturn=list.get(position)[position];
        return rturn;
    }

    @Override
    public long getItemId(int position) {
        long pos=0;


        return pos;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=new ViewHolder();
        View v = convertView;
        if (v == null) {

            v = mInflater.inflate(R.layout.icream_item, null);


            //v.setBackgroundColor(WHITE);


        }
        if(list.size()<=0)
        {

            holder.Name.setText("Não existem Gelados");
            holder.Stock.setVisibility(View.GONE);
            holder.img.setVisibility(View.GONE);
        }else {
            try {

                holder.Name = (TextView) v.findViewById(R.id.textNameIcream);
                holder.Stock = (TextView) v.findViewById(R.id.textStockIcream);
                holder.img = (ImageView) v.findViewById(R.id.imageIcream);
                holder.Name.setText(list.get(position)[0]);
                holder.Stock.setText("Em Stock:"+list.get(position)[1]);
                switch (list.get(position)[0])
                {
                    case "corneto morango":
                        holder.img.setImageResource(R.drawable.corneto_morango);
                        break;
                    case "corneto nata":
                        holder.img.setImageResource(R.drawable.corneto_nata);
                        break;
                    case "corneto limao":
                        holder.img.setImageResource(R.drawable.corneto_limao);
                        break;
                    case "fizz limao":
                        holder.img.setImageResource(R.drawable.corneto_limao);
                        break;
                    case "magnum classic":
                        holder.img.setImageResource(R.drawable.magnum_classico);
                        break;
                    case "perna de pau":
                        holder.img.setImageResource(R.drawable.perna_de_pau);
                        break;
                    case "super maxi":
                        holder.img.setImageResource(R.drawable.super_maxi);
                    break;
                    case "corneto chocolate":
                        holder.img.setImageResource(R.drawable.corneto_chocolate);
                    break;
                    case "magnum amendoa":
                        holder.img.setImageResource(R.drawable.magnum_amendoa);
                        break;
                    case "solero":
                        holder.img.setImageResource(R.drawable.solero);
                        break;
                    case "magnum branco":
                        holder.img.setImageResource(R.drawable.magnum_branco);
                        break;


                }

                holder.Name.setVisibility(View.VISIBLE);
                holder.Stock.setVisibility(View.VISIBLE);
                holder.img.setVisibility(View.VISIBLE);
            }catch (Exception e)
            {
                System.out.println("Icream Adapter:"+e);
            }




            }

        return v;
    }

    public static class ViewHolder {

        public TextView Name;
        public TextView Stock;
        public ImageView img;
    }

}

