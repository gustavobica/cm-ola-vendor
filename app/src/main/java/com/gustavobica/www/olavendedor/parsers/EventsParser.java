package com.gustavobica.www.olavendedor.parsers;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Gustavo Bica on 01/04/2016.
 */
public class EventsParser {

    private String json;
    private String url;
    private String description;
    private String title;
    private String where;
    private String when;
    private String Loc;
    JSONObject jsonObj=null;
    JSONObject nodeRoot=null;
    private String icon=null;
    private ArrayList<String[]> list=new ArrayList<String[]>();

    public EventsParser(String json) {
        this.json = json;
        try {
            jsonObj=new JSONObject(json);
            jsonObj=jsonObj.getJSONObject("events");
            JSONArray jsonA=jsonObj.getJSONArray("event");



            for(int i=0;i<jsonA.length();i++)
            {
                jsonObj=jsonA.getJSONObject(i);
                description=jsonObj.getString("description");

                if(description.equals("") || description==null || description.equals(" ") || description.equals("null"))
                {
                    description="Indisponível";
                }
                else
                {
                    description=description.replace("<br>","AQUI");
                    description=description.split("AQUI")[0];//substring(description.indexOf("<br>"));
                }

                try
                {
                    url = jsonObj.getJSONObject("image").getJSONObject("medium").getString("url");
                }
                catch(Exception e)
                {
                    //System.out.println("Url:"+e);
                    url="none";
                }
                title = jsonObj.getString("title");
                where = jsonObj.getString("city_name");//getString("region_name");
                Loc = Double.toString(jsonObj.getDouble("latitude"))+","+Double.toString(jsonObj.getDouble("longitude"));
                if(where.equals("") || where==null || where.equals("null") || where.equals(" "))
                {
                    where="Indisponível";
                }
                if((!(jsonObj.getString("venue_address").equals("")) && !(jsonObj.getString("venue_address")==null) && !(jsonObj.getString("venue_address").equals("null"))) && !where.equals("Indisponível"))
                {
                    where=where+","+jsonObj.getString("venue_address");
                }

                when = jsonObj.getString("start_time");


                list.add(i, new String[]{title,description,where,when,url,Loc});

            }

        }catch(JSONException e)
        {
            System.out.println("events parser:"+e);
        }
    }
    public String getDescription()
    {

        return description;
    }
    public ArrayList<String[]> getJson()
    {
        return list;

    }
    public String getTitle()
    {

        return title;
    }
    public double convertFhToCel(double temp)
    {
        return (((temp - 32) * 5) / 9);
    }
    public String getWhere()
    {

        return where;
    }
    public String getWhen()
    {

        return when;
    }
    public String getUrl()
    {
        return url;
    }


}


